Hypertext Markup Language (HTML) är ett standard språk för att skapa webbsidor vilken kan beskriva strukturen på en webbsida.
HTML består av en rad element och  talar om för webbläsaren hur innehållet ska visas.

Detta språk används för att kommentera texten (anteckningar för datorn) så att den kan förstå texten. Detta språk använder taggar för att definiera vilka manipulationer som ska göras på texten. De flesta märknings språk (som HTML) är läsbara för människor. Till exempel kan innehåll struktureras i en serie stycken, punktlistor eller med hjälp av bilder och datatabeller.
HTML består av flera element som används för att innesluta eller paketera olika delar av innehåll så att de ser ut på ett visst sätt eller agerar på ett visst sätt.

Omslutande taggar (meta taggar <>) kan skapa en mening paragraph ```<p>```, bild ```<img>``` eller en länk ```<a>``` som leder någon annanstans. Du kan kursivera ord ```<em>```, göra teckensnittet större ```<large>``` eller mindre ```<small>``` etc.

Semantik är meningen eller betydelse av ett ord och semantisk HTML är när vi ger våra HTML-element ett meningsfullt namn. Ett exempel är att istället för att använda ```<div>``` för att beskriva ett element, använder vi ```<main>``` eller ```<artikel>``` för att beskriva innehållet och betydelsen av elementet.